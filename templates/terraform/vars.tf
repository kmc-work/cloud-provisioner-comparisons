# ABOUT THIS FILE:
# "vars.tf" contains variable declarations that would clutter the main.tf file NOTE: Only Default Values can be set here


# This variable replaces the { get_param: "OS::stack_name" } parameter used in HEAT to add the stack name to various resource names
# Since Terraform doesn't create stacks, we can just define a custom variable
variable "prefix" {
  type    = string
  default = "PREFIX_REPLACE"
}

# The following variables are required to be declared in the root module so we can set their values in terraform.tfvars
# These are more strictly defined in their respective ./module/<MODULE NAME>/vars.tf file or directly in ./module/<MODULE_NAME>/<MODULE_NAME>.tf
variable "sec_groups" {
  type = map(any)
}

variable "nets" {
  type = map(any)
}

variable "instances" {
  type = map(any)
}
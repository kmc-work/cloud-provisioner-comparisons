# The below outputs capture the important data from the main resources of the deployed environment.
# Further filtering can be done on the JSON output from the CLI
output "security_groups_output" {
  value = module.security_groups
}

output "networks_output" {
  value = module.networks
}

output "instances_output" {
  value = module.openstack_instance
}
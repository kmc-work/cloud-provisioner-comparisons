# Each Module needs to explicitly declare what required_providers it needs
terraform {
  experiments = [module_variable_optional_attrs]

  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

# -------------------------------------------------------------------------- #
# VARIABLES ---------------------------------------------------------------- #
# -------------------------------------------------------------------------- #
# This is a module variable that stores the "STACK_NAME" prefix for use in resource naming
variable "prefix" {
  type = string
}

# This is a module variable that strictly defines the port data
variable "port_data" {
  type = object({
    port_sec_enabled = bool
    is_float         = bool
    network_name     = string
    ip               = string
  })

}

# This is a module variable that stores the current network id
variable "net_id" {
  type = string
}

# This is a module variable that stores the current security group ids
variable "sec_group_ids" {
  type = list(string)
}

# This is a module variable that stores the current subnet id
variable "sub_id" {
  type = string
}

# -------------------------------------------------------------------------- #
# RESOURCES----------------------------------------------------------------- #
# -------------------------------------------------------------------------- #
# This is a provider defined resource that creates a port security enabled port ONLY IF the port data specifies that port security IS enabled
resource "openstack_networking_port_v2" "port_sec" {
  count                 = var.port_data.port_sec_enabled ? 1 : 0
  name                  = "${var.port_data.network_name}_port_${var.prefix}"
  network_id            = var.net_id
  security_group_ids    = var.sec_group_ids == null ? [] : var.sec_group_ids
  port_security_enabled = true

  fixed_ip {
    subnet_id  = var.sub_id
    ip_address = var.port_data.ip
  }
}
# This is a provider defined resource that creates a port ONLY IF the port data specifies that port security IS NOT enabled
resource "openstack_networking_port_v2" "no_port_sec" {
  count                 = var.port_data.port_sec_enabled ? 0 : 1
  name                  = "${var.port_data.network_name}_port_${var.prefix}"
  network_id            = var.net_id
  port_security_enabled = false

  fixed_ip {
    subnet_id  = var.sub_id
    ip_address = var.port_data.ip
  }
}

# This is a provider defined resource that creates a float ip for the port ONLY IF the port data specifies that a Floating Ip is needed
resource "openstack_networking_floatingip_v2" "this" {
  count = var.port_data.is_float ? 1 : 0
  pool  = "public"
}

# This is a provider defined resource that creates a float ip for the port ONLY IF the port data specifies that a Floating Ip is needed
resource "openstack_networking_floatingip_associate_v2" "this" {
  count       = var.port_data.is_float ? 1 : 0
  floating_ip = openstack_networking_floatingip_v2.this[0].address
  port_id     = var.port_data.port_sec_enabled ? openstack_networking_port_v2.port_sec[0].id : openstack_networking_port_v2.no_port_sec[0].id
}

# -------------------------------------------------------------------------- #
# OUTPUTS ------------------------------------------------------------------ #
# -------------------------------------------------------------------------- #
output "port_out" {
  # The value of this output is dependent on whether or not port security was enabled on the port
  value = var.port_data.port_sec_enabled ? openstack_networking_port_v2.port_sec[0] : openstack_networking_port_v2.no_port_sec[0]
}


output "float_ip_out" {
  value = openstack_networking_floatingip_v2.this
}
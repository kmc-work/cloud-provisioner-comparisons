# Each Module needs to explicitly declare what required_providers it needs
terraform {
  experiments = [module_variable_optional_attrs]

  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

# -------------------------------------------------------------------------- #
# VARIABLES ---------------------------------------------------------------- #
# -------------------------------------------------------------------------- #
# This is a module variable that stores the "STACK_NAME" prefix for use in resource naming
variable "prefix" {
  type = string
}

# This is a module variable that holds a list of sec groups to apply to an instance(port)
variable "instance_sec_groups" {
  type = list(string)
}

# This is a module variable that stores the network data passed from the network module
variable "network_data" {
  type = map(any)
}

# This is a module variable that strictly defines the information needed to configure an instance
variable "instance_data" {
  type = object(
    {
      name                 = string
      image                = string
      flavor               = string
      security_groups      = optional(list(string))
      config_file_location = string
      ports = list(object(
        {
          port_sec_enabled = bool
          is_float         = bool
          network_name     = string
          ip               = optional(string)
        }
      ))
    }
  )
}



# -------------------------------------------------------------------------- #
# RESOURCES----------------------------------------------------------------- #
# -------------------------------------------------------------------------- #
# This module is the only module in this example that calls upon it's own submodule
module "instance_ports" {
  source        = "./ports"
  count         = length(var.instance_data.ports)
  port_data     = var.instance_data.ports[count.index]
  net_id        = var.network_data["${var.instance_data.ports[count.index].network_name}"].network_out.id
  sec_group_ids = var.instance_sec_groups
  sub_id        = var.network_data["${var.instance_data.ports[count.index].network_name}"].subnet_out.id
  prefix        = var.prefix
}

# This data block is a special type of "template_file" that has an attribute called "rendered"
# that can be used to pass a rendered script or cloud-config.yml to an instance's "user_data" field
data "template_file" "user_data" {
  template = file("${var.instance_data.config_file_location}")
}

# This is a provider defined resource that create an instance with a dynamic number of networks connected
resource "openstack_compute_instance_v2" "this" {
  name         = "${var.instance_data.name}_${var.prefix}"
  image_name   = var.instance_data.image
  flavor_name  = var.instance_data.flavor
  config_drive = true
  user_data    = data.template_file.user_data.rendered

  dynamic "network" {
    # for_each = var.instance_data.ports
    for_each = module.instance_ports
    content {
      port = network.value.port_out.id
    }
  }
}

# -------------------------------------------------------------------------- #
# OUTPUTS ------------------------------------------------------------------ #
# -------------------------------------------------------------------------- #

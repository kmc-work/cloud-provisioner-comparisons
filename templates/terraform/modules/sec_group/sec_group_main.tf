# Each Module needs to explicitly declare what required_providers it needs
terraform {
  experiments = [module_variable_optional_attrs]

  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

# -------------------------------------------------------------------------- #
# VARIABLES ---------------------------------------------------------------- #
# -------------------------------------------------------------------------- #
# This is a module variable that strictly defines the type and parameters needed by the object.
variable "sec_group" {
  type = object(
    {
      name        = string
      description = string
      rules = list(object(
        {
          description      = optional(string)
          ethertype        = string
          direction        = string
          protocol         = optional(string)
          port_range_min   = optional(number)
          port_range_max   = optional(number)
          remote_ip_prefix = optional(string)
      }))
    }

  )
}

# -------------------------------------------------------------------------- #
# RESOURCES----------------------------------------------------------------- #
# -------------------------------------------------------------------------- #
# This is a provider defined resource that creates a Security Group
resource "openstack_networking_secgroup_v2" "secgroup" {
  name        = var.sec_group.name
  description = var.sec_group.description

}
# This is a provider defined resource that adds each rule(s) to the appropriate Security Group
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule" {
  count             = length(var.sec_group.rules)
  direction         = var.sec_group.rules[count.index].direction
  ethertype         = var.sec_group.rules[count.index].ethertype
  protocol          = var.sec_group.rules[count.index].protocol
  port_range_min    = var.sec_group.rules[count.index].port_range_min
  port_range_max    = var.sec_group.rules[count.index].port_range_max
  description       = var.sec_group.rules[count.index].description
  remote_ip_prefix  = var.sec_group.rules[count.index].remote_ip_prefix
  security_group_id = openstack_networking_secgroup_v2.secgroup.id
}

# -------------------------------------------------------------------------- #
# OUTPUTS ------------------------------------------------------------------ #
# -------------------------------------------------------------------------- #
output "sec_group_out" {
  value = openstack_networking_secgroup_v2.secgroup
}

output "sg_rules_out" {
  value = openstack_networking_secgroup_rule_v2.secgroup_rule
}
# ABOUT THIS FILE:
# "main.tf" is the top-level configuration file in what's referred to as the "root module" of our Terraform example.
# With a few special exceptions, Terraform doesn't care whether there is one file or many in a module directory, as long as they end in *.tf
# they will compile to single internal configuration before being applied to the cloud.

# The "best practice" for root-modules or even large sub-modules is to contain the following:
# .
# ├── main.tf               Typically contains the main configuration logic and sub-module calls
# └── vars.tf               Contains variable declarations that would clutter the main.tf file NOTE: Only Default Values can be set here
# ├── terraform.tfvars      A special Terraform file that is used to set custom values to the variables defined in vars.tf NOTE: STDIN and ENV Variables can also be used
# ├── outputs.tf            Any resource information (like floating IP addresses) that you want access to after the resources are created are typically defined in a separate file

# ----------------------------- PROVIDER INFO : START ------------------------------------------------------------------------------------------ #
# This block identifies any special features that this terraform module will require,
# as well as which required providers are needed to be downloaded to this module's directory
terraform {
  # This line is required to be able to define "optional" variables as seen in vars.tf
  experiments = [module_variable_optional_attrs]
  # The terraform providers can be found at https://registry.terraform.io.
  # They are written in Go and will handle all of the API calls necessary to create the resources defined in the configuration
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }

  backend "http" {
  }
}

# The following configuration options can be set instead of using a project.rc file or clouds.yaml,
# however, since our configuration is meant to be able to be deployed to multiple projects, we are using
# a project.rc file to set the ENV variables that terraform will be able to recognize
provider "openstack" {

  # user_name   = "admin"
  # tenant_name = "admin"
  # password    = "pwd"
  # auth_url    = "http://myauthurl:5000/v2.0"
  # region      = "RegionOne"
}
# ----------------------------- PROVIDER INFO : END ------------------------------------------------------------------------------------------ #


# ----------------------------- RESOURCE CREATION : START ------------------------------------------------------------------------------------ #

# --- Neutron Router Resource
# This 'data' block reaches out to the VTA and set's the id of the "public" network to the variable: data.openstack_networking_network_v2.this.id
# which can be used later in the configuration
data "openstack_networking_network_v2" "this" {
  name = "public"
}

# This is a provider defined resource that creates a router to connect the public cloud to the private networks defined later in the configuration
resource "openstack_networking_router_v2" "external_router" {
  name                = "router_${var.prefix}"
  external_network_id = data.openstack_networking_network_v2.this.id
}

# --- Security Group Resources
# This is a custom written module that will build Security Group related resources as defined in 'terraform.tfvars'
# All of the provider defined resources are located in files identified under the "source" argument of the module
module "security_groups" {
  source    = "./modules/sec_group"
  for_each  = var.sec_groups
  sec_group = each.value
}


# --- Network Resources
# Another custom module that builds all of the Networks that will attach to the router
module "networks" {
  source   = "./modules/network"
  for_each = var.nets
  networks = each.value
  router   = openstack_networking_router_v2.external_router.id
  prefix   = var.prefix
}

# Instances
# Another custom module that builds all of the Instances and Ports
module "openstack_instance" {
  source              = "./modules/instances"
  for_each            = var.instances
  instance_data       = each.value
  network_data        = module.networks
  instance_sec_groups = [for x in each.value.security_groups : module.security_groups[x].sec_group_out.id]
  prefix              = var.prefix
}
# ----------------------------- RESOURCE CREATION : END ------------------------------------------------------------------------------------ #
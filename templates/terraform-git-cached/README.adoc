= Terraform (Using Git Cache)

Terraform is a declarative tool that uses a proprietary language (HashiCorp Language) to describe resources and their relationships.
The syntax is a cross between a generic programming language and JSON, but is more readable and easier to write.
Terraform also saves the state of the environment and all relevant metadata which makes modification and updates easier and more reliable.

At the end of the day, Terraform and Heat share many of the same capabilities. The code below will look very similar to the code used in the previous section. However there are some key differences.

* Terraform is platform agnostic. A few lines at the top of config file will specify which provider resources are available to use.
* Terraform Integrates with far more platforms than Heat and the community and documentation is far more extensive.
* While Terraform doesn't have all the benefits of an Imperative language, it does provide many functions that make dynamically creating and managing resources easier.
* More information can be found https://www.terraform.io/intro/vs/cloudformation[HERE]

After playing around with Terraform for a few days, it was easy to see why it's so popular in the industry.

=== Key Files
----
.
├── config
│   └── ansible_config.yaml
├── modules
│   ├── instances
│   │   ├── ports
│   │   │   └── ports_main.tf
│   │   └── instance_main.tf
│   ├── network
│   │   └── network_main.tf
│   └── sec_group
│       └── sec_group_main.tf
├── init.sh
├── main.tf
├── outputs.tf
├── README.ADOC
├── terraform.tfvars
└── vars.tf
----

NOTE: Each file in this demo has been heavily commented to explain individual syntax elements, but I will cover the parts that create the instance group as demonstrated in the previous sections.

.main.tf
----
# Instances
# Another custom module that builds all of the Instances and Ports
module "openstack_instance" {
  source              = "./modules/instances"
  for_each            = var.instances
  instance_data       = each.value
  network_data        = module.networks
  instance_sec_groups = [for x in each.value.security_groups : module.security_groups[x].sec_group_out.id]
  prefix              = var.prefix
}
----

* Resources in Terraform are typically called "modules". Here I'm specifying the path to the custom instance module. I'm also using the `for_each` function to iterate over the instances in the `instances` variable defined in `vars.tf`. We can also see the much cleaner and established "dot-notation" syntax for accessing nested data, as well as a one-line for loop to iterate over the security group data.

.modules/instances/instance_main.tf
----
module "instance_ports" {
  source        = "./ports"
  count         = length(var.instance_data.ports)
  port_data     = var.instance_data.ports[count.index]
  net_id        = var.network_data["${var.instance_data.ports[count.index].network_name}"].network_out.id
  sec_group_ids = var.instance_sec_groups
  sub_id        = var.network_data["${var.instance_data.ports[count.index].network_name}"].subnet_out.id
  prefix        = var.prefix
}

# This data block is a special type of "template_file" that has an attribute called "rendered"
# that can be used to pass a rendered script or cloud-config.yml to an instance's "user_data" field
data "template_file" "user_data" {
  template = file("${var.instance_data.config_file_location}")
}

# This is a provider defined resource that create an instance with a dynamic number of networks connected
resource "openstack_compute_instance_v2" "this" {
  name         = "${var.instance_data.name}_${var.prefix}"
  image_name   = var.instance_data.image
  flavor_name  = var.instance_data.flavor
  config_drive = true
  user_data    = data.template_file.user_data.rendered

  dynamic "network" {
    # for_each = var.instance_data.ports
    for_each = module.instance_ports
    content {
      port = network.value.port_out.id
    }
  }
}
----

* This custom instance module calls a sub-module called `instance_ports` which is responsible for creating the ports for the instance. All of the instance fields are clearly left-justified and map to their corresponding values stored as a dictionary object in `terrafom.tfvars`. The `dynamic` function is used to create a dynamic number of networks connected to the instance, which is a nice built in feature of Terraform. Overall I find the syntax very readable with only minor critiques that I'll cover in the PROs / CONS section.

.terraform.tfvars
----
# This variable contains a map of instances and their corresponding configuratin information
instances = {
  ctfd = {
    name                 = "ctfd"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = ["ctfd-security-group"]
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = true
        is_float         = true
        network_name     = "ctfd"
        ip               = null
      }
    ]
  }
  jumpbox_01 = {
    name                 = "jumpbox_01"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = ["disable-sec-group", "ctfd-security-group"]
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = true
        is_float         = true
        network_name     = "main"
        ip               = "172.16.0.1"
      }
    ]
  }
----

* This is only a small portion of the dictionary object storing all of the instance information. Again, I find this just as readable as any of the YAML files we looked at earlier, and Terraform has the best variable calling syntax we've seen so far.

=== PROs and CONs

As powerful and refined an option as Terraform is, it's still a bit of a learning curve. Some personal critiques I have are:

*Referencing a nested dictionary object is a bit of a hard to keep track of sometimes.*

`var.network_data["${var.instance_data.ports[count.index].network_name}"].subnet_out.id`

*There is no native if statement in Terraform, so I had to use a ternary operator to check if the resource should be created.*

`count = var.port_data.port_sec_enabled ? 1 : 0` is how to do a basic if statement (that builds 1 resource if the port is enabled, and 0 if it is not.)


* PROS
** Portability between cloud solutions.
** Easy to read declarative syntax.
** Dynamic modularity.

* CONS
** Unique language standard.
** Lacking robust programmatic logic. (By design...but it does make certain aspects of the language more difficult to use.)

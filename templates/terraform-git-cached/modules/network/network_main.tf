# Each Module needs to explicitly declare what required_providers it needs
terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
  }
}

# -------------------------------------------------------------------------- #
# VARIABLES ---------------------------------------------------------------- #
# -------------------------------------------------------------------------- #
# This is a module variable that stores the "STACK_NAME" prefix for use in resource naming
variable "prefix" {
  type = string
}
# This is a module variable that stores the router id
variable "router" {
  type = string
}
# This is a module variable that strictly defines the networks variable information
variable "networks" {
  type = object({
    name       = string
    cidr       = string
    gateway_ip = string
    start      = string
    end        = string
  })
}

# -------------------------------------------------------------------------- #
# RESOURCES----------------------------------------------------------------- #
# -------------------------------------------------------------------------- #
# This is a provider defined resource that creates a network
resource "openstack_networking_network_v2" "net" {
  name = "net_${var.networks.name}_${var.prefix}"
}

# This is a provider defined resource that creates a subnet for each network
resource "openstack_networking_subnet_v2" "subnet" {
  name       = "subnet_${var.networks.name}_${var.prefix}"
  network_id = openstack_networking_network_v2.net.id
  gateway_ip = var.networks.gateway_ip
  cidr       = var.networks.cidr

  allocation_pool {
    start = var.networks.start
    end   = var.networks.end
  }
}

# This is a provider defined resource that creates a router interface for each subnet
resource "openstack_networking_router_interface_v2" "router_interface" {
  router_id = var.router
  subnet_id = openstack_networking_subnet_v2.subnet.id
}

# -------------------------------------------------------------------------- #
# OUTPUTS ------------------------------------------------------------------ #
# -------------------------------------------------------------------------- #
output "network_out" {
  value = openstack_networking_network_v2.net
}

output "subnet_out" {
  value = openstack_networking_subnet_v2.subnet
}
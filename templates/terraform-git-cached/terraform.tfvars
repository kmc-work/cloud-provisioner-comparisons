# --- SECURITY GROUP INFORMATION
# This variable contains a map of security groups to create and the corresponding rules to apply
sec_groups = {
  disable-sec-group = {
    name        = "disable-sec-group"
    description = "This security group allows all traffic to all instances"
    rules = [
      {
        description      = ""
        direction        = "ingress"
        ethertype        = "IPv4"
        protocol         = "tcp"
        port_range_min   = 1
        port_range_max   = 65535
        remote_ip_prefix = "0.0.0.0/0"
      },
      {
        description      = ""
        direction        = "ingress"
        ethertype        = "IPv4"
        protocol         = "icmp"
        port_range_min   = 1
        port_range_max   = 65535
        remote_ip_prefix = "0.0.0.0/0"
      },
      {
        description      = ""
        direction        = "ingress"
        ethertype        = "IPv4"
        protocol         = "udp"
        port_range_min   = null
        port_range_max   = null
        remote_ip_prefix = "0.0.0.0/0"
      }
    ]
  }

  ctfd-security-group = {
    name        = "ctfd-security-group"
    description = "This security group only allows TCP traffic on ports 8000 and 1175"
    rules = [
      {
        description      = ""
        direction        = "ingress"
        ethertype        = "IPv4"
        protocol         = "tcp"
        port_range_min   = 8000
        port_range_max   = 8000
        remote_ip_prefix = "0.0.0.0/0"
      },
      {
        description      = ""
        direction        = "ingress"
        ethertype        = "IPv4"
        protocol         = "tcp"
        port_range_min   = 1775
        port_range_max   = 1775
        remote_ip_prefix = "0.0.0.0/0"
      },
      {
        description      = ""
        direction        = "ingress"
        ethertype        = "IPv4"
        protocol         = "icmp"
        port_range_min   = null
        port_range_max   = null
        remote_ip_prefix = "0.0.0.0/0"
      }
    ]
  }
}

# --- NETWORK VARIABLES
# This variable contains a map of networks and their corresponding configuration info
nets = {
  ctfd = {
    name       = "ctfd"
    cidr       = "172.16.30.0/27"
    gateway_ip = "172.16.30.30"
    start      = "172.16.30.1"
    end        = "172.16.30.29"
  }
  main = {
    name       = "main"
    cidr       = "172.16.0.0/25"
    gateway_ip = "172.16.0.126"
    start      = "172.16.0.110"
    end        = "172.16.0.120"
  }
  alpha = {
    name       = "alpha"
    cidr       = "192.168.0.0/27"
    gateway_ip = "192.168.0.30"
    start      = "192.168.0.24"
    end        = "192.168.0.29"
  }
  bravo = {
    name       = "bravo"
    cidr       = "10.10.0.64/26"
    gateway_ip = "10.10.0.126"
    start      = "10.10.0.114"
    end        = "10.10.0.120"
  }
  charlie = {
    name       = "charlie"
    cidr       = "192.168.1.128/25"
    gateway_ip = "192.168.1.254"
    start      = "192.168.1.240"
    end        = "192.168.1.250"
  }
}

# --- INSTANCE INFORMATION
# This variable contains a map of instances and their corresponding configuratin information
instances = {
  ctfd = {
    name                 = "ctfd"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = ["ctfd-security-group"]
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = true
        is_float         = true
        network_name     = "ctfd"
        ip               = null
      }
    ]
  }
  jumpbox_01 = {
    name                 = "jumpbox_01"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = ["disable-sec-group", "ctfd-security-group"]
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = true
        is_float         = true
        network_name     = "main"
        ip               = "172.16.0.1"
      }
    ]
  }

  jumpbox_02 = {
    name                 = "jumpbox_02"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = []
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = false
        is_float         = true
        network_name     = "main"
        ip               = "172.16.0.2"
      }
    ]
  }

  jumpbox_03 = {
    name                 = "jumpbox_03"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = []
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = false
        is_float         = true
        network_name     = "main"
        ip               = "172.16.0.3"
      }
    ]
  }

  alpha_1 = {
    name                 = "alpha_1"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = []
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = false
        is_float         = false
        network_name     = "main"
        ip               = "172.16.0.66"
      },
      {
        port_sec_enabled = false
        is_float         = false
        network_name     = "alpha"
        ip               = "192.168.0.14"
      }
    ]
  }

  alpha_2 = {
    name                 = "alpha_2"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = []
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = false
        is_float         = false
        network_name     = "alpha"
        ip               = "192.168.0.1"
      }
    ]
  }

  bravo_1 = {
    name                 = "bravo_1"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = []
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = false
        is_float         = false
        network_name     = "main"
        ip               = "172.16.0.101"
      },
      {
        port_sec_enabled = false
        is_float         = false
        network_name     = "bravo"
        ip               = "10.10.0.111"
      }
    ]
  }

  bravo_2 = {
    name                 = "bravo_2"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = []
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = false
        is_float         = false
        network_name     = "bravo"
        ip               = "10.10.0.77"
      }
    ]
  }

  charlie_1 = {
    name                 = "charlie_1"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = []
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = false
        is_float         = false
        network_name     = "main"
        ip               = "172.16.0.92"
      },
      {
        port_sec_enabled = false
        is_float         = false
        network_name     = "charlie"
        ip               = "192.168.1.207"
      }
    ]
  }

  charlie_2 = {
    name                 = "charlie_2"
    image                = "cirros"
    flavor               = "disk.small"
    security_groups      = []
    config_file_location = "./config/ansible_config.yaml"
    ports = [
      {
        port_sec_enabled = false
        is_float         = false
        network_name     = "charlie"
        ip               = "192.168.1.182"
      }
    ]
  }
}

# --- Requirements to Run --- #
# python3 -m venv .
# source ./bin/activate
# for dep in pyyaml jinja2 markupsafe;do ./bin/easy_install $dep;done
# --------------------------- #

from jinja2 import Environment, FileSystemLoader
import yaml

# create an env object
env = Environment(loader = FileSystemLoader('./'), trim_blocks=True, lstrip_blocks=True)


# Load data from YAML file into Python dictionary
config = yaml.load(open('vars.yaml'), Loader=yaml.FullLoader)


# Load Jinja2 template
template = env.get_template('main.j2')

# Render template using data and print the output
open("main.yaml", 'w').write(template.render(config))


# --- pylint Error Message Rules
# pylint: disable=wildcard-import
# pylint: disable=line-too-long
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
# pylint: disable=unused-variable


"""An OpenStack Python Pulumi program"""

import pulumi
import pulumi_openstack as openstack

# The lines below are required to pull in the configuration variables as declared in
# the Pulumi.demo.yaml file. These are the main variables that define what
# will get built out by the functions in this program.
config = pulumi.Config()
sec_groups = config.require_object("sec_groups")
nets = config.require_object("nets")
instances = config.require_object("instances")

# The name of the Pulumi Stack to uniquely identify resources
STACK_NAME = pulumi.get_stack()

# ----------------------------- FUNCTION DECLARATIONS  : START ---------------------------------- #
def create_security_groups(sec_group_data):
    """Dynamically Creates an Openstack Sec Group given the data from test-project:sec_groups in Pulumi.demo.yaml

    Args:
        sec_group_data (dict): Nested Dictionary containing all of the defined security groups and associated rules

    Returns:
        sec_group_output (dict): Simple dictionary that stores the created security group objects for reference by other resources
    """

    # Create a dictionary containing relevant output used by other resources
    sec_group_output = {}

    # This for loop will create Openstack Security Groups from the data stored in Pulumi.demo.yaml
    for sec_group_name, sec_group_dict in sec_group_data.items():

        # Create the Security Group
        secgroup = openstack.networking.SecGroup(
            sec_group_name,
            name=f"{sec_group_name}_{STACK_NAME}",
            description=sec_group_dict["description"],
        )

        # This list will hold Openstack SecGroupRule objects create by
        # the set_sec_group_rules function below. The list isn't used as anything
        # other then a container of the objects. The creation of the objects
        # is all that is required for pulumi to create the resource.
        sec_group_rules_list = []

        # This for loop iterates over the keys of each security_group and only takes action
        # on the "rules" key which contains a list of rules
        for key in sec_group_dict.keys():
            if key == "rules":
                # This for loop iterates over the list of rules and passes the relevant data to
                # the set_sec_group_rules function which actually creates the pulumi rules object
                for rule in sec_group_dict[key]:
                    sec_group_rules_list.append(set_sec_group_rules(rule, secgroup.id))

        # Pulumi Stack Outputs
        pulumi.export(f"{sec_group_name}_id_out", secgroup.id)
        pulumi.export(f"{sec_group_name}_out", secgroup)

        # Save the id of the sec_group created by the for loop to the output dictionary
        sec_group_output[sec_group_name] = secgroup.id
    return sec_group_output


def set_sec_group_rules(rule_dict, sec_group_id):
    """Creates an Openstack SecGroupRule given a dictionary of rule values

    Args:
        rule_dict (dict): Dictionary object storing lists of Security Group Rules
        sec_group_id      (str): The ID of the security group currently being added to

    Returns:
        rule_object (openstack.networking.SecGroupRule): A Pulumi openstack object containing a SecGroup Rule
    """

    # Creates a Openstack SecGroupRule Object with relevant parameters
    rule_object = openstack.networking.SecGroupRule(
        f"rule-{rule_dict['name']}",
        direction=rule_dict["direction"],
        ethertype=rule_dict["ethertype"],
        protocol=rule_dict["protocol"],
        port_range_min=rule_dict["port_range_min"],
        port_range_max=rule_dict["port_range_max"],
        description=rule_dict["description"],
        remote_ip_prefix=rule_dict["remote_ip_prefix"],
        security_group_id=sec_group_id,
    )
    return rule_object


def create_networks(network_data):
    """Create an Openstack Network, Subnet, and Router Interface for each private network in the range

    Args:
        network_data (dict): Dictionary object storing all of the networking related information

    Returns:
        network_output (dict): Dictionary containing network and subnet ids for use by other resources
    """

    # Create a dictionary containing relevant output used by other resources
    network_output = {}

    # This for loop will create a network, subnet, and router interface from the data stored in Pulumi.demo.yaml
    for net_name, net_dict in network_data.items():

        network = openstack.networking.Network(
            f"net_{net_name}",
            name=f"{net_name}_{STACK_NAME}",
        )

        subnet = openstack.networking.Subnet(
            f'subnet_{net_dict["name"]}',
            name=f"{net_dict['name']}_{STACK_NAME}",
            network_id=network.id,
            gateway_ip=net_dict["gateway_ip"],
            cidr=net_dict["cidr"],
            allocation_pools=[
                {
                    "start": net_dict["start"],
                    "end": net_dict["end"],
                }
            ],
        )

        # This python variable is never referenced by anything else in the
        # program, but each instance of it in the for loop will create a
        # unique router interface object that Pulumi will use to create
        # the corresponding resource.
        router_interface = openstack.networking.RouterInterface(
            f'router_interface_{net_dict["name"]}',
            router_id=external_router.id,
            subnet_id=subnet.id,
        )

        # Pulumi Stack Outputs
        pulumi.export(f"{net_name}_id_out", network.id)
        pulumi.export(f"{net_name}_out", network)

        # Save the network and subnet ID to a dictionary to be used by other resources
        network_output[net_name] = {"network": network.id, "subnet": subnet.id}
    return network_output


def create_ports(instance_data, net_objects, sec_group_objects):
    """Creates Openstack port objects and returns them to be referenced by the appropriate instance.
        Will check if the port is a floating port or has port security enabled and create the appropriate resources.

    Args:
        instance_data        (dict): Dictionary object storing all of the instance related information
        net_objects          (dict): Dictionary object storing all of the networking related information
        sec_group_objects    (dict): Dictionary object storing all of the security group related information

    Returns:
        ports_output (dict): Dictionary containing lists of the ports created that need to be attached to a specific instance
    """

    # Create a dictionary containing relevant output used by other resources
    ports_output = {}

    # This for loop iterates over the keys in each individual instance dictionary and
    # filters the information related to the ports of that instance
    for ports in instance_data.keys():

        # Create two list to store the port object and name
        ports_list = []
        port_names = []
        if ports == "ports":

            # This for loop will iterate over each port in the list of ports for an instance
            # and check whether or not porte security is enabled or if it is floating ip port
            # and create the appropriate resource. Then it will save the port info to lists
            # that are returned a dictionary and used as input by the create_instances function
            for net_port in instance_data[ports]:
                net_name = net_port["network_name"]
                port_name = f'{instance_data["name"]}_port_{net_name}'

                if net_port["port_sec_enabled"] is True:
                    sec_group_list = []

                    # This for loop populates a list of security groups that will be associated with the
                    # current port
                    for sec_group in instance_data["security_groups"]:
                        sec_group_list.append(sec_group_objects[sec_group])

                    # Create a port that has port security enabled
                    port = openstack.networking.Port(
                        port_name,
                        name=f"{net_name}_port_{STACK_NAME}",
                        security_group_ids=sec_group_list,
                        network_id=net_objects[net_name]["network"],
                        port_security_enabled=True,
                        fixed_ips=[
                            {
                                "subnet_id": net_objects[net_name]["subnet"],
                                "ip_address": net_port["ip"],
                            }
                        ],
                    )
                    ports_list.append(port)
                    port_names.append(port_name)
                else:

                    # Create a port if port security is not enabled
                    port = openstack.networking.Port(
                        port_name,
                        name=f"{net_name}_port_{STACK_NAME}",
                        network_id=net_objects[net_name]["network"],
                        port_security_enabled=False,
                        fixed_ips=[
                            {
                                "subnet_id": net_objects[net_name]["subnet"],
                                "ip_address": net_port["ip"],
                            }
                        ],
                    )
                    ports_list.append(port)
                    port_names.append(port_name)

                if net_port["is_float"] is True:

                    # If the port has a floating point IP assigned to it, then
                    # create the relevant resources
                    float_ip = openstack.networking.FloatingIp(
                        f"{port_name}_float",
                        pool="public",
                    )
                    float_ip_assoc = openstack.networking.FloatingIpAssociate(
                        f"{port_name}_float_assoc",
                        floating_ip=float_ip.address,
                        port_id=port.id,
                    )
                    pulumi.export(f"float_ip_{port_name}", float_ip.address)

            # Create a dictionary object with all of the port related information for use by
            # the create_instances function
            ports_output[instance_data["name"]] = {
                "ids": ports_list,
                "name": port_names,
            }

    return ports_output


def create_instances(instance_data, net_objects, sec_group_objects):
    """This function takes most of the objects created by the other functions and builds an instance
        that is connected to the proper networks.

    Args:
        instance_data        (dict): Dictionary object storing all of the instance related information
        net_objects          (dict): Dictionary object storing all of the networking related information
        sec_group_objects    (dict): Dictionary object storing all of the security group related information
    """

    # This for loop will create Openstack Instance objects for each instance listed in Pulumi.demo.yaml
    for instance_name, instance_dict in instance_data.items():

        # Save the contents of the config file as a string variable to pass to the
        # instance's user_data
        with open(instance_dict["config_file_location"], "r", encoding="utf-8") as f:
            config_file = f.read()

        # Create the port objects relevant to this instance
        port_objects = create_ports(instance_dict, net_objects, sec_group_objects)

        # Make a list of the port ids to pass to the instance's network parameter
        port_ids = []

        # These nested for loops create special InstanceNetworkArgs objects that are
        # required by the instance's network parameter
        for x in port_objects.values():
            for i in x["ids"]:
                port_ids.append(openstack.compute.InstanceNetworkArgs(port=i.id))

        # Create the instance object with relevant parameters
        instance = openstack.compute.Instance(
            f"{instance_name}_{STACK_NAME}",
            image_name=instance_dict["image"],
            flavor_name=instance_dict["flavor"],
            config_drive=True,
            user_data=config_file,
            networks=port_ids,
        )


# ----------------------------- FUNCTION DECLARATIONS  : END ---------------------------------- #

# ----------------------------- RESOURCE CREATION : START --------------------------------------- #

# The majority of the resources are created in the functions defined above. As long as the Pulumi.demo.yaml
# config data is formatted correctly, then the functions should dynamically generate resources as you
# test and adjust the values depending on your range requirements


# --- Neutron Router Resource

# The get_network function used below with actually return the id of the "public" network that
# already exist in the openstack environment
pub_net = openstack.networking.get_network(name="public")

# Create an Openstack Neutron Router to connect all the private networks and router interfaces to.
external_router = openstack.networking.Router(
    f"externalRouter_{STACK_NAME}",
    name=f"externalRouter_{STACK_NAME}",
    external_network_id=pub_net.id,
)

# --- Security Group Resources

# Creates all of the Security Groups and saves the needed output objects for use by the create_instances function
sec_group_resources = create_security_groups(sec_groups)

# --- Network Resources
# Creates all of the Networks and saves the needed output objects for use by the create_instances function
net_resources = create_networks(nets)


# --- Instances Resources

# Creates the instances based on all of the data and objects created above
create_instances(instances, net_resources, sec_group_resources)


# ----------------------------- RESOURCE CREATION : END ----------------------------------------- #

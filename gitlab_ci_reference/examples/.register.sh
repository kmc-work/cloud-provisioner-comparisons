# --- Build Docker images with CI/CD with Shell executor --- #


# To include Docker commands in your CI/CD jobs, you can configure your runner to use the shell executor.
# In this configuration, the gitlab-runner user runs the Docker commands, but needs permission to do so.

# 1. Install GitLab Runner.
# 2. Register a runner. Select the shell executor. For example:

sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token REGISTRATION_TOKEN \
  --executor shell \
  --description "My Runner"
￼
# 3. On the server where GitLab Runner is installed, install Docker Engine. View a list of supported platforms.
# 4. Add the gitlab-runner user to the docker group:

sudo usermod -aG docker gitlab-runner

# 5. Verify that gitlab-runner has access to Docker:

sudo -u gitlab-runner -H docker info

# 6. In GitLab, to verify that everything works, add docker info to .gitlab-ci.yml:
    # before_script:
    #   - docker info

    # build_image:
    #   script:
    #     - docker build -t my-docker-image .
    #     - docker run my-docker-image /script/to/run/tests



# --- Oneliner Registration --- #
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "PROJECT_REGISTRATION_TOKEN" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --tag-list "docker,aws" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

sudo gitlab-runner register \
     --config /tmp/test-config.toml \
     --template-config /tmp/test-config.template.toml \
     --non-interactive \
     --url https://gitlab.com \
     --registration-token __REDACTED__ \
     --name test-runner \
     --tag-list shell,test \
     --locked \
     --paused \
     --executor shell


# --- Use SSH Keys in CI/CD --- #
before_script:
##
## Install ssh-agent if not already installed, it is required by Docker.
## (change apt-get to yum if you use an RPM-based image)
##
- 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'

##
## Run ssh-agent (inside the build environment)
##
- eval $(ssh-agent -s)

##
## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
## We're using tr to fix line endings which makes ed25519 keys work
## without extra base64 encoding.
## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
##
- echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -

##
## Create the SSH directory and give it the right permissions
##
- mkdir -p ~/.ssh
- chmod 700 ~/.ssh

##
## Optionally, if you will be using any Git commands, set the user name and
## and email.
##
# - git config --global user.email "user@example.com"
# - git config --global user.name "User name"